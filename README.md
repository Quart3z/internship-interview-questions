# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON

2. Tell us about a newer (less than five years old) web technology you like and why?

3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that?

4. Explain this block of code in Big-O notation
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation. 

6. In Javascript, What is a "closure"? How does JS Closure works?

7. In Javascript, what is the difference between var, let, and const. When should I them?